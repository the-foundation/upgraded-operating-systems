#!/bin/bash
_buildx_arch()           { case "$(uname -m)" in aarch64) echo linux/arm64;; x86_64) echo linux/amd64 ;; armv7l|armv7*) echo linux/arm/v7;; armv6l|armv6*) echo linux/arm/v6;;  esac ; } ;
export OSVER=alpine
export DROPBEAR_VERSION=$(curl -s "https://github.com/TheFoundation/hardened-dropbear/releases"|grep expanded_assets|grep 'src="https://github.com/'|sed 's/.\+expanded_assets\/v//g;s/".\+//g'|head -n1);
export DOWNLOAD_URL="https://github.com/TheFoundation/hardened-dropbear/releases/download/v$DROPBEAR_VERSION/hardened-dropbear-$OSVER."$(_buildx_arch |sed 's~/~_~g')

( cd /
echo "trying binary install $DROPBEAR_VERSION from $DOWNLOAD_URL"
curl -Lkv "${DOWNLOAD_URL}"|tar xvz
)

dropbear --help 2>&1|grep -q -e assword -e ed25 || (
which apk && apk add ccache gcc make autoconf ca-certificates git zlib-dev libc-dev

test -e /tmp/.beardef.h  && rm /tmp/.beardef.h  

beartarget=/tmp/dropbear
cd /tmp/ 
test -e "$beartarget" ||  git clone https://github.com/mkj/dropbear.git $beartarget 
test -e "$beartarget" && cd  "$beartarget" && git pull 



cd "$beartarget/src/" && echo varsetup && ( 
echo '
#define DROPBEAR_AES128 0
#define DROPBEAR_DEFAULT_RSA_SIZE 4096
#define DROPBEAR_DSS 0
#define DROPBEAR_AES256 1
#define DROPBEAR_TWOFISH256 0
#define DROPBEAR_TWOFISH128 0
#define DROPBEAR_CHACHA20POLY1305 1
#define DROPBEAR_ENABLE_CTR_MODE 1
#define DROPBEAR_ENABLE_CBC_MODE 0
#define DROPBEAR_ENABLE_GCM_MODE 1
#define DROPBEAR_MD5_HMAC 0
#define DROPBEAR_SHA1_HMAC 0
#define DROPBEAR_SHA2_256_HMAC 1
#define DROPBEAR_SHA1_96_HMAC 0
#define DROPBEAR_ECDSA 1
#define DROPBEAR_DH_GROUP14_SHA1 0
#define DROPBEAR_DH_GROUP14_SHA256 1
#define DROPBEAR_DH_GROUP16 0
#define DROPBEAR_CURVE25519 1
#define DROPBEAR_ECDH 0
#define DROPBEAR_DH_GROUP1 0
#define DROPBEAR_DH_GROUP1_CLIENTONLY 0
' |grep -v ^$ > /tmp/.bear_configvars )
test -e /tmp/.bear_configvars || exit 10

cd /$beartarget || exit 10

echo -n replacing" " && for var in $(cut -d" " -f2 /tmp/.bear_configvars);do 
        echo "|"$var"|";
        sed 's/.\+'$var'.\+//g' $beartarget/src/default_options.h -i ;done && (
            mv $beartarget/src/default_options.h /tmp/.beardef.h 
        )

test -e /tmp/.beardef.h || exit 20

( cat /tmp/.beardef.h  /tmp/.bear_configvars  ) >  $beartarget/src/default_options.h 
tail -n 1 "/$beartarget/src/default_options.h"|grep -q "$(tail -n 1 /tmp/.bear_configvars)" || echo "SNIPPET NOT FOUND..exit"
tail -n 1 "/$beartarget/src/default_options.h"|grep -q "$(tail -n 1 /tmp/.bear_configvars)" || exit 30

export PREFIX=/usr
export CC='ccache gcc'
export HOST=bouncing-dropbear-hardened

echo CONFIG
( cd "/$beartarget"; autoconf  &&  autoheader  &&  ccache ./configure --prefix=$PREFIX --enable-plugin  --enable-bundled-libtom  )|sed 's/$/ → /g'|tr -d '\n'  
echo BUILD
time ( cd "/$beartarget" ;  make PROGRAMS="dropbear dbclient dropbearkey dropbearconvert " -j$(($(nproc)+2)) 2>/tmp/builderr 1>/tmp/buildlog )
echo INSTALL
PFX=""
which sudo  && {  (id -u|grep ^0$ ) || PFX=sudo ; } ;
installresult=$(bash -c $PFX' make PROGRAMS="dropbear dbclient dropbearkey dropbearconvert scp"   SCPPROGRESS=1  install ' 2>&1)
)

echo "#####"
echo "##### BUILD OUTPUT"
echo "###############################################"
echo "$installresult"
echo "###############################################"

KEEPFILES=$(echo "$installresult"|grep ^install|grep -v '^install -'|while read line;do file=$(echo "$line"|cut -d" " -f2);dir=$(echo "$line"|cut -d" " -f3);echo $dir"/"$file;done )
echo "$KEEPFILES"
ln -s  /usr/local/sbin/dropbear /usr/local/bin/dbclient         /usr/local/bin/dropbearconvert  /usr/local/bin/dropbearkey /usr/sbin   ;     rm -rf $beartarget 2>/dev/null || true
apk del gcc make autoconf git zlib-dev libc-dev
test -f /usr/libexec/sftp-server || (mkdir -p /usr/libexec/ && ln -s /usr/lib/ssh/sftp-server /usr/libexec/sftp-server)
echo "su -s /bin/bash www-data" > /usr/bin/wwwsh;chmod +x /usr/bin/wwwsh
grep wwwsh /root/.bashrc || echo 'alias wwwsh="su -s /bin/bash www-data"' >> /root/.bashrc
