#!/bin/bash
_do_cleanup() {

        #remove build packages
        ##### remove all packages named *-dev* or *-dev:* (e.g. mylib-dev:amd64 )
      which apk &>/dev/null && apk update
      which apt-get &>/dev/null && _fix_apt_keys
      which apt-get &>/dev/null && apt-get update && apt-get upgrade -y
      which apt-get &>/dev/null && apt-get clean -y && apt-get autoclean -y && apt-get autoremove -y
      removeselector=$( dpkg --get-selections|grep -v deinstall$|cut -f1|cut -d" " -f1|grep -e ^cpan -e ^dbus$ -e shared-mime-info  -e adwaita-icon-theme -e ubuntu-mono -e gsettings-desktop-schemas -e python-software-properties -e software-properties-common  -e ^make -e ^build-essential -e \-dev: -e \-dev$ -e ^texlive-base -e  ^doxygen  -e  ^libllvm   -e ^gcc -e ^g++ -e ^build-essential -e \-dev: -e \-dev$ |grep -v ^gcc.*base)
        [[ -z "${removeselector}" ]] || { echo "deleting "${removeselector}" " ; apt-get purge -y ${removeselector} 2>&1 | sed 's/$/|/g'|tr -d '\n' ; } ;
      which apt-get &>/dev/null &&  ( apt-get clean -y && apt-get autoclean -y && apt-get autoremove -y ) 2>&1 |  sed 's/$/|/g'|tr -d '\n'
        #remove doc and man and /tmp
        deleteselector=$(find /tmp/ /usr/share/vim/vim*/doc/ /var/cache/debconf/*-old /var/lib/apt/lists/ /var/cache/man     /usr/share/texmf/ /usr/local/share/doc /usr/share/doc /usr/share/man -mindepth 1 -type f 2>/dev/null  ) &
        [[ -z "${deleselector}" ]] || find /tmp/ /usr/share/vim/vim*/doc/ /var/cache/debconf/*-old /var/lib/apt/lists/ /var/cache/man     /usr/share/texmf/ /usr/local/share/doc /usr/share/doc /usr/share/man -mindepth 1 -type f -delete 2>/dev/null
      find /tmp/ -type d -mindepth 1 -delete &
        ##remove ssh host keys
        for keyz in $(ls -1 /etc/ssh/ssh_host_*key /etc/ssh/ssh_host_*pub 2>/dev/null) /etc/dropbear/dropbear_dss_host_key /etc/dropbear/dropbear_rsa_host_key /etc/dropbear/dropbear_ecdsa_host_key ;do
                 test -f "${keyz}" && rm "${keyz}" & done

        wait

        ##remove package manager caches
        (which apt-get 2>/dev/null && apt-get autoremove -y --force-yes &&  apt-get clean && find -name "/var/lib/apt/lists/*_*" -delete )| sed 's/$/|/g'|tr -d '\n'

echo ; } ;


##########################################


_do_cleanup_quick() {
_do_cleanup;
echo ; } ;

    echo -n "::DROBEAR INSTALL:APT:"


        apt-get update ; apt-get install -y dropbear-bin curl
        ## check if the already installed dropbear has "disable-weak-ciphers" support
        dropbear --help 2>&1 |grep -q ed255 || { echo "no ed25519 found - using dropbear from git " ;
                                                 apt-get install -y build-essential git zlib1g-dev gcc make autoconf libc-dev pkg-config shtool   || exit 111
                                                 apt remove -y dropbear-bin ; } ;

    




_buildx_arch()           { case "$(uname -m)" in aarch64) echo linux/arm64;; x86_64) echo linux/amd64 ;; armv7l|armv7*) echo linux/arm/v7;; armv6l|armv6*) echo linux/arm/v6;;  esac ; } ;

export OSVER=$((head -n1 /etc/lsb-release;echo "-" ;head -n3 /etc/lsb-release |tail -n1)|cut -d "=" -f2|tr  '[:upper:]' '[:lower:]'|tr -d '\n')
#export DROPBEAR_VERSION=$(curl -s "https://github.com/TheFoundation/hardened-dropbear/releases"|grep expanded_assets|grep 'src="https://github.com/'|sed 's/.\+expanded_assets\/v//g;s/".\+//g'|head -n1);
export DROPBEAR_VERSION=$(curl -s "https://github.com/TheFoundation/hardened-dropbear/releases"|grep expanded_assets|grep 'src="https://github.com/'|sed 's/.\+expanded_assets\/v//g;s/".\+//g'|head -n1);
export DOWNLOAD_URL="https://github.com/TheFoundation/hardened-dropbear/releases/download/v$DROPBEAR_VERSION/hardened-dropbear-$OSVER."$(_buildx_arch |sed 's~/~_~g')".tar.gz"

( cd /
echo "trying binary install $DROPBEAR_VERSION from $DOWNLOAD_URL"
curl -Lkv "${DOWNLOAD_URL}"|tar xvz
)

dropbear --help 2>&1|grep -q -e assword -e ed25 || {
beartarget=/tmp/dropbear
echo "building dropbear on $(uname -m)"
    cd /tmp/ &&  git clone https://github.com/mkj/dropbear.git $beartarget && cd $beartarget && echo varsetup && echo '
#define DROPBEAR_AES128 0
#define DROPBEAR_DEFAULT_RSA_SIZE 4096
#define DROPBEAR_DSS 0
#define DROPBEAR_AES256 1
#define DROPBEAR_TWOFISH256 0
#define DROPBEAR_TWOFISH128 0
#define DROPBEAR_CHACHA20POLY1305 1
#define DROPBEAR_ENABLE_CTR_MODE 1
#define DROPBEAR_ENABLE_CBC_MODE 0
#define DROPBEAR_ENABLE_GCM_MODE 1
#define DROPBEAR_MD5_HMAC 0
#define DROPBEAR_SHA1_HMAC 0
#define DROPBEAR_SHA2_256_HMAC 1
#define DROPBEAR_SHA1_96_HMAC 0
#define DROPBEAR_ECDSA 1
#define DROPBEAR_DH_GROUP14_SHA1 0
#define DROPBEAR_DH_GROUP14_SHA256 1
#define DROPBEAR_DH_GROUP16 0
#define DROPBEAR_CURVE25519 1
#define DROPBEAR_ECDH 0
#define DROPBEAR_DH_GROUP1 0
#define DROPBEAR_DH_GROUP1_CLIENTONLY 0
    ' > $beartarget/configvars 
    echo -n replacing" " && for var in $(cut -d" " -f2 $beartarget/configvars);do 
        echo $var;sed 's/.\+'$var'.\+//g' /tmp/dropbear/src/default_options.h -i ;done 
        cat /$beartarget/configvars >>  /$beartarget/src/default_options.h 

echo -n "DROPBEAR_CONFG:$(uname -m):"
cd "/$beartarget" && (autoconf  &&  autoheader  && ./configure )|sed 's/$/ → /g'|tr -d '\n'  
echo -n "DROPBEAR_BUILD:$(uname -m):"
cd "/$beartarget" &&  make PROGRAMS="dropbear dbclient dropbearkey dropbearconvert scp " -j$(($(nproc)+2))  &&  make PROGRAMS="dropbear dbclient dropbearkey dropbearconvert scp"  SCPPROGRESS=1 install || exit 222
ln -s  /usr/local/sbin/dropbear /usr/local/bin/dbclient         /usr/local/bin/dropbearconvert  /usr/local/bin/dropbearkey /usr/sbin/   ;
rm -rf $beartarget 2>/dev/null || true
echo -n ; } ;

    
    test -f /usr/libexec/sftp-server || (mkdir -p /usr/libexec/ && ln -s /usr/lib/sftp-server /usr/libexec/sftp-server)
    echo "su -s /bin/bash www-data" > /usr/bin/wwwsh;chmod +x /usr/bin/wwwsh
    grep wwwsh /root/.bashrc || echo 'alias wwwsh="su -s /bin/bash www-data"' >> /root/.bashrc


        rm -rf /tmp/dropbear 2>/dev/null || true
    ##### remove all packages named -dev or -dev: (e.g. mylib-dev:amd64 )
    apt-get purge -y build-essential git zlib1g-dev gcc make autoconf libc-dev pkg-config manpages manpages-dev shtool  $( dpkg --get-selections|grep -v deinstall$|cut -f1|cut -d" " -f1|grep  -e \-dev: -e \-dev$ ) 2>&1 | sed 's/$/|/g'|tr -d '\n'
    apt-get -y autoremove 2>&1 | sed 's/$/|/g'|tr -d '\n'
    ( find /tmp/ -mindepth 1 -type f -delete 2>/dev/null || true  &  find /tmp/ -mindepth 1 -type d -delete 2>/dev/null || true   ) &
    ( find /usr/share/doc -type f -delete || true & find  /usr/share/man -type f -delete || true  ) &
    wait
    ##remove package manager caches
    which apt-get 2>/dev/null && apt-get autoremove -y --force-yes || true  &&  apt-get clean && find -name "/var/lib/apt/lists/*_*" -delete
    ##remove ssh host keys
    for keyz in /etc/dropbear/dropbear_dss_host_key /etc/dropbear/dropbear_rsa_host_key /etc/dropbear/dropbear_ecdsa_host_key ;do test -f $keyz && rm $keyz;done

_do_cleanup
ls -lh1 /usr/local/sbin/dropbear /usr/sbin/dropbear
echo "PATHd ropbear:"
dropbear --help 2>&1 |grep -q ed255  && (echo dropbear OK ;exit 0) || (echo no dropbear ;exit 23)
