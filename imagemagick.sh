#!/bin/bash
_do_cleanup() {

        #remove build packages
        ##### remove all packages named *-dev* or *-dev:* (e.g. mylib-dev:amd64 )
      _fix_apt_keys
      apt-get update && apt-get upgrade -y
      apt-get clean -y && apt-get autoclean -y && apt-get autoremove -y
      removeselector=$( dpkg --get-selections|grep -v deinstall$|cut -f1|cut -d" " -f1|grep -e ^cpan -e ^dbus$ -e shared-mime-info  -e adwaita-icon-theme -e ubuntu-mono -e gsettings-desktop-schemas -e python-software-properties -e software-properties-common  -e ^make -e ^build-essential -e \-dev: -e \-dev$ -e ^texlive-base -e  ^doxygen  -e  ^libllvm   -e ^gcc -e ^g++ -e ^build-essential -e \-dev: -e \-dev$ |grep -v ^gcc.*base)
        [[ -z "${removeselector}" ]] || { echo "deleting "${removeselector}" " ; apt-get purge -y ${removeselector} 2>&1 | sed 's/$/|/g'|tr -d '\n' ; } ;
      which apt-get &>/dev/null &&  ( apt-get clean -y && apt-get autoclean -y && apt-get autoremove -y ) 2>&1 |  sed 's/$/|/g'|tr -d '\n'
        #remove doc and man and /tmp
        deleteselector=$(find /tmp/ /usr/share/vim/vim*/doc/ /var/cache/debconf/*-old /var/lib/apt/lists/ /var/cache/man     /usr/share/texmf/ /usr/local/share/doc /usr/share/doc /usr/share/man -mindepth 1 -type f 2>/dev/null  ) &
        [[ -z "${deleselector}" ]] || find /tmp/ /usr/share/vim/vim*/doc/ /var/cache/debconf/*-old /var/lib/apt/lists/ /var/cache/man     /usr/share/texmf/ /usr/local/share/doc /usr/share/doc /usr/share/man -mindepth 1 -type f -delete 2>/dev/null
      find /tmp/ -type d -mindepth 1 -delete &
        ##remove ssh host keys
        for keyz in $(ls -1 /etc/ssh/ssh_host_*key /etc/ssh/ssh_host_*pub 2>/dev/null) /etc/dropbear/dropbear_dss_host_key /etc/dropbear/dropbear_rsa_host_key /etc/dropbear/dropbear_ecdsa_host_key ;do
                 test -f "${keyz}" && rm "${keyz}" & done

        wait

        ##remove package manager caches
        (which apt-get 2>/dev/null && apt-get autoremove -y --force-yes &&  apt-get clean && find -name "/var/lib/apt/lists/*_*" -delete )| sed 's/$/|/g'|tr -d '\n'

echo ; } ;

#################################
apt_install_depends() {
    local pkg="$1"
    apt-get update
    apt-get -y install -s "$pkg" \
      | sed -n \
        -e "/^Inst $pkg /d" \
        -e 's/^Inst \([^ ]\+\) .*$/\1/p' \
      | xargs apt-get install
    _do_cleanup || true
echo ; } ;
apt-remove-build-dep() { ### found here: https://askubuntu.com/questions/180504/how-can-i-remove-all-build-dependencies-for-a-particular-package
    packageName="$1"
    apt-mark auto $(apt-cache showsrc $packageName | sed -e '/Build-Depends/!d;s/Build-Depends: \|,\|([^)]*),*\|\[[^]]*\]//g' | sed -E 's/\|//g; s/<.*>//g')
    apt-mark manual build-essential fakeroot devscripts
    apt autoremove --purge
echo ; } ;
##################################

## IMAGICK WEBP
    which convert &>/dev/null || ( apt-get update  &>/dev/null && apt-get -y --no-install-recommends install imagemagick ) |sed 's/$/|/g'|tr -d '\n'
    build_imagick=false
    convert --help|grep webp || build_imagick=true
    if [ "${build_imagick}" = "true" ] ;then
    echo "building imagick"
    apt-get -y purge imagemagick 2>&1 | sed 's/$/|/g'|tr -d '\n'


    ## IMagick builds only with WEBP libwebp
    apt-get update && apt-get -y install wget
    WEBPARCHIVE=$(wget -O- https://storage.googleapis.com/downloads.webmproject.org/releases/webp/index.html|grep "href"|sed 's/.\+\<a href="\/\///g'|cut -d\" -f1|grep libwebp-[0-9]|grep tar.gz|grep [0-9].tar.gz$|grep -v -e mac -e linux -e rc1 -e rc2 -e rc3 -e rc4 -e rc5 |tail -n1)
    echo ":Build:libwebp: FROM"  "${WEBPARCHIVE}"
    sed -i '/deb-src/s/^# //' /etc/apt/sources.list && apt update && apt-get -y build-dep imagemagick && apt-get -y install util-linux wget build-essential gcc make autoconf libc-dev pkg-config libjpeg-dev libpng-dev
    dpkg --configure -a

     cd /tmp/ && wget -q -c -O- "${WEBPARCHIVE}" | tar xvz || exit 111


    ( cd $(find /tmp/ -type d -name "libwebp-*" |head -n1) &&  ./configure 2>&1  |sed 's/$/ → /g'|tr -d '\n'
    make -j $(nproc) 2>&1  |sed 's/$/ → /g'|tr -d '\n'
    make install  2>&1 ) || exit 111

    ### IMAGICK
    IMAGICURL=https://imagemagick.org/download/ImageMagick.tar.gz
    IMAGICURL=https://imagemagick.org/archive/ImageMagick.tar.gz
    apt-get update && apt-get install -y util-linux && apt-get -y build-dep imagemagick && cd /tmp/ && wget $IMAGICURL && tar xvzf ImageMagick.tar.gz|| exit 222
    /bin/bash -c 'cd $(find /tmp/ -type d -name "ImageMagick-*" |head -n1) && ./configure  --with-webp=yes '"|sed 's/$/ → /g'|tr -d '\n' "' && make -j$(nproc) && make install && ldconfig /usr/local/lib &&  ( find /tmp/ -name "ImageMagic*" |xargs rm -rf  )'
    apt-get -y  purge build-essential gcc make autoconf libc-dev pkg-config || true
    apt-get -y  purge $(dpkg --get-selections |grep -v deinsta |grep  -e ^texlive -e ^tex-common -e ^ubuntu-mono -e ^x11-common -e ^ghostscript -e ^humanity-icon-theme |cut -f1)
    apt-get -y purge ghostscript
    apt-remove-build-dep imagemagick
    apt-get -y autoremove

    #apt-install-depends imagemagick
    apt-get -y install $(apt-cache search libxml|grep ^libxml[0-9]|cut -d" " -f1|grep [0-9]$) netpbm $(apt-cache search libfontconfig|grep ^libfontconfig[0-9]|cut -d" " -f1|grep [0-9]$) $(apt-cache search liblcms|grep ^liblcms[0-9]|cut -d" " -f1|grep [0-9]$) $(apt-cache search libpangocairo|grep ^libpangocairo-[0-9]|cut -d" " -f1|grep [0-9]$) $(apt-cache search libopenexr|grep ^libopenexr[0-9]|cut -d" " -f1|grep [0-9]$)  $(apt-cache search libfftw|grep ^libfftw[0-9]|cut -d" " -f1|grep bin$)  $(apt-cache search liblqr|grep ^liblqr|cut -d" " -f1|grep -v 'dev')  $(apt-cache search libgomp|grep ^libgomp[0-9]|cut -d" " -f1|grep -v '-') libwmf-bin $(apt-cache search libdjvul|grep ^libdjvulibre[0-9]|cut -d" " -f1) 2>&1 | sed 's/$/|/g'|tr -d '\n'
    apt_install_depends imagemagick
    for deleteme in  /usr/share/icons /var/cache/man  /usr/bin/luatex /usr/local/share/doc /usr/local/share/man /usr/share/icons /usr/bin/doxygen /usr/share/texlive/  /usr/share/texmf/ /usr/share/doc /usr/local/share/doc /usr/share/man ;do
             ( find ${deleteme} -mindepth 1 -type f -delete || true  ) &
    done
  find /tmp/ -mindepth 1 -delete
    _do_cleanup
    fi
    echo "final test for imagick"
    identify -version || exit 333
