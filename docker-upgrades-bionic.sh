#!/bin/bash
_do_cleanup() {

        #remove build packages
        ##### remove all packages named *-dev* or *-dev:* (e.g. mylib-dev:amd64 )
      _fix_apt_keys
      apt-get update && apt-get upgrade -y
      apt-get clean -y && apt-get autoclean -y && apt-get autoremove -y
      removeselector=$( dpkg --get-selections|grep -v deinstall$|cut -f1|cut -d" " -f1|grep -e ^cpan -e ^dbus$  -e adwaita-icon-theme -e ubuntu-mono -e gsettings-desktop-schemas -e python-software-properties -e software-properties-common  -e ^make -e ^build-essential -e \-dev: -e \-dev$ -e ^texlive-base -e  ^doxygen  -e  ^libllvm   -e ^gcc -e ^g++ -e ^build-essential -e \-dev: -e \-dev$ |grep -v ^gcc.*base)
        [[ -z "${removeselector}" ]] || { echo "deleting "${removeselector}" " ; apt-get purge -y ${removeselector} 2>&1 | sed 's/$/|/g'|tr -d '\n' ; } ;
      which apt-get &>/dev/null &&  ( apt-get clean -y && apt-get autoclean -y && apt-get autoremove -y ) 2>&1 |  sed 's/$/|/g'|tr -d '\n'
        #remove doc and man and /tmp
        deleteselector=$(find /tmp/ /usr/share/vim/vim*/doc/ /var/cache/debconf/*-old /var/lib/apt/lists/ /var/cache/man     /usr/share/texmf/ /usr/local/share/doc /usr/share/doc /usr/share/man -mindepth 1 -type f 2>/dev/null  ) &
        [[ -z "${deleselector}" ]] || find /tmp/ /usr/share/vim/vim*/doc/ /var/cache/debconf/*-old /var/lib/apt/lists/ /var/cache/man     /usr/share/texmf/ /usr/local/share/doc /usr/share/doc /usr/share/man -mindepth 1 -type f -delete 2>/dev/null
      find /tmp/ -type d -mindepth 1 -delete &
        ##remove ssh host keys
        for keyz in $(ls -1 /etc/ssh/ssh_host_*key /etc/ssh/ssh_host_*pub 2>/dev/null) /etc/dropbear/dropbear_dss_host_key /etc/dropbear/dropbear_rsa_host_key /etc/dropbear/dropbear_ecdsa_host_key ;do
                 test -f "${keyz}" && rm "${keyz}" & done

        wait

        ##remove package manager caches
        (which apt-get 2>/dev/null && apt-get autoremove -y --force-yes &&  apt-get clean && find -name "/var/lib/apt/lists/*_*" -delete )| sed 's/$/|/g'|tr -d '\n'

echo ; } ;


##########################################


_do_cleanup_quick() {
  _do_cleanup;
echo ; } ;


##  systemd hurts and prevents configuration of certain packages, it will not easily run as it does hav PID 1 so we just let it say systemtrue and let it rot
test -f /bin/systemctl  && ( rm /bin/systemctl && dpkg-divert --add /bin/systemctl && ln -sT /bin/true /bin/systemctl ) || true


apt-get update && apt-get -y dist-upgrade && apt-get -y --no-install-recommends install ssl-cert lftp  iputils-ping less net-tools lsof sysstat  netcat unzip socat
apt-get install -y  --no-install-recommends  dirmngr software-properties-common || true  && export  LC_ALL=C.UTF-8 && LC_ALL=C.UTF-8 apt-get update && LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php  && LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/apache2 && LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/pkg-gearman && apt-get update &&  apt-get -y dist-upgrade &&  apt-get purge -y software-properties-common && apt-get autoremove -y --force-yes || true

##### remove all packages named -dev or -dev: (e.g. mylib-dev:amd64 )

removeselector=$(dpkg --get-selections |grep -v deinstall|cut -f1 |grep -e dev:i386 -e dev:amd64 -e '\-dev$' -e '\-dev:' -e python -e ^libicu -e ^perl)
apt-get purge -y $removeselector build-essential python3 python ubuntu-mono gcc make $( dpkg --get-selections|grep -v deinstall$|cut -f1|cut -d" " -f1|grep  -e \-dev: -e \-dev$ ) 2>&1 | sed 's/$/|/g'|tr -d '\n'

_do_cleanup
exit 0
